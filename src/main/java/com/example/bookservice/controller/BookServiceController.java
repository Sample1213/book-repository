package com.example.bookservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookservice.entity.Book;
import com.example.bookservice.service.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Api(value = "Book-Service Api")
@RestController
@RequestMapping("/book")
public class BookServiceController {
	
	@Autowired
	private BookService bookService;
	
	@GetMapping("/getBooks")
	@ApiOperation(value = "Get list of books ", response =List.class)
	public ResponseEntity<List<Book>> getAllBooks(){
		List<Book> listOfBooks=bookService.getAllBooks();
		return new ResponseEntity(listOfBooks,HttpStatus.OK);
	}
	
	
	@GetMapping("/getBook/{bookId}")
	@ApiOperation(value="Get Single Book With BookId")
	public ResponseEntity<Book> getBook(@ApiParam(name="bookId")@PathVariable Long bookId){
		Optional<Book> book=bookService.getBook(bookId);
		return new ResponseEntity(book.get(),HttpStatus.OK);
	}
	
	
	@PostMapping("/saveBook")
	@ApiOperation(value="Save Book")
	public ResponseEntity<Book> saveBookDetails(@RequestBody Book book){
		Book savedBook=bookService.saveBookDetails(book);
		return new ResponseEntity<Book>(savedBook, HttpStatus.CREATED);
	}
	
	@PutMapping("/updateBook/{bookId}")
	@ApiOperation(value="Update Book With BookId")
	public ResponseEntity<Book> updateBookDetails(@PathVariable Long bookId,@RequestBody Book book){
		try {
		Book updatedBook=bookService.updateBookDetails(bookId, book);
		return new ResponseEntity<Book>(updatedBook,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/deleteBook/{bookId}")
	public ResponseEntity<Void> deleteBook(@PathVariable Long bookId) {
		bookService.deleteBook(bookId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
