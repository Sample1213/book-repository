package com.example.bookservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GeneratorType;

import io.swagger.annotations.ApiModelProperty;

@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long bookId;
	
	@ApiModelProperty(notes = "Name of the book",name="bookName",required=true,value="test name")
	private String bookName;
    
	@ApiModelProperty(notes = "Author of the book",name="bookAuthor",required=true)
	private String bookAuthor;
    
	@ApiModelProperty(notes = "Category of the book",name="bookCategory",required=true)
	private String bookCategoy;
    
	@ApiModelProperty(notes = "Description of the book",name="bookDescription",required=true)
	private String bookDescription;
	
	public Book() {
		
	}

	public Book(String bookName, String bookAuthor, String bookCategoy, String bookDescription) {
		super();
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.bookCategoy = bookCategoy;
		this.bookDescription = bookDescription;
	}

	public long getBookId() {
		return bookId;
	}

	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public String getBookCategoy() {
		return bookCategoy;
	}

	public void setBookCategoy(String bookCategoy) {
		this.bookCategoy = bookCategoy;
	}

	public String getBookDescription() {
		return bookDescription;
	}

	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", bookAuthor=" + bookAuthor + ", bookCategoy="
				+ bookCategoy + ", bookDescription=" + bookDescription + "]";
	}
	

}
