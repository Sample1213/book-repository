package com.example.bookservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bookservice.entity.Book;
import com.example.bookservice.repository.BookRepository;

@Service
public class BookService {
	
	@Autowired
	private BookRepository bookRepository;
	
	@PostConstruct
	public void init() {
		List<Book> listOfBooks=new ArrayList<>();
		listOfBooks.add(new Book("Wings Of Fire", "Arun Tiwari","Social", "An Autobiography of Abdul kalam"));
		listOfBooks.add(new Book("The Theory Of EvereyThing", "Stephen Hawking","Social", "introduction to the subjects of cosimology"));
		bookRepository.saveAll(listOfBooks);
	}
	
	
	public List<Book> getAllBooks(){
		return bookRepository.findAll();
	}
	
	
	public Book saveBookDetails(Book book) {
		return bookRepository.save(book);

	}
	
	public Optional<Book> getBook(Long id) {
		return bookRepository.findById(id);
		
	}
	
	
	public Book updateBookDetails(Long bookId,Book book)throws RuntimeException  {
		
		Optional<Book> existedBook=bookRepository.findById(bookId);
		if(existedBook.isPresent()) {
		book.setBookId(bookId);
		return bookRepository.save(book);
		}
		else {
			throw new RuntimeException(bookId+"Book Does Not Exist");
		}
		
	}
	
	public void deleteBook(Long bookId) {
		bookRepository.deleteById(bookId);
	}
	

}
